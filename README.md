# Qboxmail webmail translations

This repository contains all translations of the Qboxmail webmail.
If some label is not correct in your language, please fork the repo and submit a pull request.
If your language is not still present, you can contribute pushing a new file using **en.js** as master, and if you want, you can ask to be the maintainer of that language.

### NOTES:

1. Every time a new label is added, the same label will be added in all language files in English, so it is up to the maintainer of the language to keep it updated.
2. Before writing us to correct a label in your language, please write to the maintainer that will help you and will make the proper fix.
3. The translations are based on JSON syntax, so make sure the JSON is valid. You can use a validator tool like [jsonlint.com](https://jsonlint.com/)
4. **Before submit a pull request**, run `node check.js` to check if you have covered all translations. If you don't have NodeJS intalled you can do it downloading the installer from [nodejs.org](https://nodejs.org/)

This is the list of language and the related maintainers:

| Language | Maintainer | Contact                                                          |
| -------- | ---------- | ---------------------------------------------------------------- |
| de       | Stefan     | [stefan@piloly.com](mailto:stefan@piloly.com) [@piloly](@piloly) |
| en       | Qboxmail   | [info@qboxmail.com](mailto:info@qboxmail.com)                    |
| es       |            |                                                                  |
| it       | Qboxmail   | [info@qboxmail.com](mailto:info@qboxmail.com)                    |
| nl       |            |                                                                  |
| pt       |            |                                                                  |
| sv       | Thomas     | [@tmikaeld](@tmikaeld)                                           |

If you would like to become a maintainer, please [write to us](mailto:info@qboxmail.com)
